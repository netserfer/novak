/*//= ../../node_modules/jquery/dist/jquery.js*/
var $animatingElements = $('#teaser').find('[data-animation]');
var $window = $(window),
	$document = $(document),
	_windowH = $window.height(),
	_windowW = $window.width(),
	_windowST = $window.scrollTop();

/* Global objects */
var $html = $('html'),
	$body = $('body');

/* Update global variables when window is resized */
$window.on('load resize', function() {
	_windowH = $window.height();
	_windowW = $window.width();
	_windowST = $window.scrollTop();
});

/* Update scrollTop on window scroll */
$window.on('scroll', function() {
	_windowST = $window.scrollTop();
});

function doAnimations(elements) {
	var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	elements.each(function() {
		var $this = $(this);
		var $animationDelay = $this.data('delay');
		var $animationType = 'animated ' + $this.data('animation');
		$this.css({
			'animation-delay': $animationDelay,
			'-webkit-animation-delay': $animationDelay
		});
		$this.addClass($animationType).one(animationEndEvents, function() {
			$this.removeClass($animationType);
		});
	});
};
function check_arrows(slider, to) {
	if(to == 0){
		$(slider).closest('.subnav').find('.prev').addClass('disabled');
		$(slider).closest('.subnav').find('.next').removeClass('disabled');
	}else if(to == 2){
		$(slider).closest('.subnav').find('.prev').removeClass('disabled');
		$(slider).closest('.subnav').find('.next').addClass('disabled');
	}else{
		$(slider).closest('.subnav').find('.prev').removeClass('disabled');
		$(slider).closest('.subnav').find('.next').removeClass('disabled');
	}
}
function clothes_slider_init(slider, pager) {
	const clothes_slider_bx = $(slider).bxSlider({
		pagerCustom: pager,
		responsive: true,
		controls: false,
		touchEnabled:false,
		pause: 5000,
		mode: 'fade',
		onSlideBefore: function ($ele, from, to) {
			clothes_slider_bx.goToSlide(to);
			$(pager).find('a').removeClass('active');
			$ele.addClass('active');
			check_arrows(pager, to);
		},
		onSlideAfter: function($slideElement, oldIndex, newIndex) {
			$(pager).next('.digit_big').text('0'+(newIndex+1));
		},
		onSliderLoad: function($slideElement, oldIndex, newIndex) {
			$(slider).closest('.img_big').find('.next').click(function () {
				clothes_slider_bx.goToNextSlide();
				return false;
			});
			$(slider).closest('.img_big').find('.prev').click(function () {
				clothes_slider_bx.goToPrevSlide();
				return false;
			});
			//тык по картинке справа
			setTimeout(function () {
				$('.style_group_slide.active .item_image').click(function () {
					i = $(this).data('slide');
					console.log(i);
					clothes_slider_bx.goToSlide(i);
					return false;
				});
			});
			check_arrows(pager, 0);
		}
	});
};
$(function() {
	var match = window.location.pathname.match('[^/]+(?=\.html)')
	if (match !== null && ['bye'].includes(match[0]))
		$('body').addClass('second_page');

	function digits_slider_init(slider, pager) {
		const digits_slider_bx = $(slider).bxSlider({
			pagerCustom: pager,
			auto: true,
			mode: 'vertical',
			autoControls: true,
			controls: false,
			touchEnabled:false,
			pause: 10000,
			onSlideBefore: function ($ele, from, to) {
				digits_slider_bx.goToSlide(to);
				$(pager).find('a').removeClass('active');
				$ele.addClass('active');
				check_arrows(slider, to);
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex) {
				$(pager).next('.digit_big').text('0'+(newIndex+1));
				$('#selected_style .clothing_item').css({'background-image': "url('" + $slideElement.data('source') + "')"});
			},
			onSliderLoad: function($slideElement, oldIndex, newIndex) {
				$(slider).closest('.subnav').find('.next').click(function () {
					digits_slider_bx.goToNextSlide();
					return false;
				});
				$(slider).closest('.subnav').find('.prev').click(function () {
					digits_slider_bx.goToPrevSlide();
					return false;
				});
				check_arrows(slider, 0);
			}
		});
	}

	//как собрать свой образ
	const choose_slider_bx = $('#choose_slider').bxSlider({
		pagerCustom: '#choose_pager',
		auto: true,
		controls: false,
		touchEnabled:false,
		pause: 5000,
		mode: 'fade',
		onSlideBefore: function ($ele, from, to) {
			choose_slider_bx.goToSlide(to);
			$('#choose_pager a').removeClass('active');
			$ele.addClass('active');
			var $animatingElements = $('#choose_slider .active').find('[data-animation]');
			doAnimations($animatingElements);
		},
		onSliderLoad: function($slideElement, oldIndex, newIndex) {
			var $animatingElements = $('#choose_slider').find('[data-animation]');
			doAnimations($animatingElements);
		}
	});

	//Выбери свой стиль:
	const selected_slider_bx = $('#selected_slider').bxSlider({
		pagerCustom: '#selected_style_pager',
		//auto: true,
		controls: false,
		touchEnabled:false,
		mode: 'fade',
		onSlideBefore: function ($ele, from, to) {
			selected_slider_bx.goToSlide(to);
			$('#selected_style_pager a, #selected_slider .slide').removeClass('active');
			$ele.addClass('active');

			var $animatingElements = $('#selected_slider .active').find('[data-animation]');
			doAnimations($animatingElements);
		},
		onSlideAfter: function($slideElement, oldIndex, newIndex) {
			var c_type = $('#selected_style_pager .active').text();
			$('#bye').attr('href', '/bye.html?type=' + c_type);
			//цифры слайдер справа:
			digits_slider_init('#selected_style .active .digits_slider', '#selected_style .active .digits_pager');
			url = $('#selected_style .active .digits_slider .active').data('source');
			$('#selected_style .clothing_item').css({'background-image': "url('" + url + "')"});
		},
		onSliderLoad: function($slideElement, oldIndex, newIndex) {
			var $animatingElements = $('#selected_slider .slide:eq(0)').find('[data-animation]');
			doAnimations($animatingElements);

			var c_type = $('#selected_style_pager .item:eq(0)').text();
			$('#bye').attr('href', '/bye.html?style_type=' + c_type);

			digits_slider_init('#selected_style .slide:eq(0) .digits_slider', '#selected_style .slide:eq(0) .digits_pager');
			url = $('#selected_style .slide:eq(0) .digits_slider .active').data('source');
			$('#selected_style .clothing_item').css({'background-image': "url('" + url + "')"});
			//скролл к слайду и перереход
			$('.toggle-slick').on('click', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('html, body').animate({
					scrollTop: $(target).offset().top
				}, 2000);
				var slide = $(this).attr('data-slide');
				selected_slider_bx.goToSlide(slide);
			});
		}
	});

	//Преимущества предложения
	const ability_slider_bx = $('#ability_slider').bxSlider({
		pagerCustom: '#ability_pager',
		auto: true,
		controls: false,
		touchEnabled:false,
		pause: 5000,
		mode: 'fade',
		onSlideBefore: function ($ele, from, to) {
			ability_slider_bx.goToSlide(to);
			$('#ability_pager a').removeClass('active');
			$ele.addClass('active');
			var $animatingElements = $('#ability_slider .active').find('[data-animation]');
			doAnimations($animatingElements);
		},
		onSliderLoad: function($slideElement, oldIndex, newIndex) {
			var $animatingElements = $('#ability_slider').find('[data-animation]');
			doAnimations($animatingElements);
		}
	});

	//style_group_slider
	const style_group_slider_bx = $('#style_group_slider').bxSlider({
		pagerCustom: '#style_group_pager',
		controls: false,
		touchEnabled:false,
		mode: 'fade',
		onSliderLoad: function($slideElement, oldIndex, newIndex) {
			$('#current_style').text($('#style_group_pager .item').eq(0).text());
			clothes_slider_init('#style_group_slider .style_group_slide:eq(0) .clothes', '#style_group_slider .style_group_slide:eq(0) .digits_pager');
			var params = location.search.replace('?', '').split('=');
			$('#style_group_slider .style_group_slide:eq(0)').addClass('active');
			if(params[0]){
				active_style_type = params[1];
			}else{
				active_style_type = 'free';
			}
			slide_num = $('#style_group_pager a:contains("' + active_style_type +'")').index();
			setTimeout(function () {
				style_group_slider_bx.goToSlide(slide_num);
			}, 500);
		},
		onSlideBefore: function ($ele, from, to) {
			style_group_slider_bx.goToSlide(to);
			$('#style_group_pager a, #style_group_slider .style_group_slide').removeClass('active');
			$ele.addClass('active');
		},
		onSlideAfter: function($slideElement, oldIndex, newIndex) {
			$('#current_style').text($('#style_group_pager .active').text());
			clothes_slider_init('#style_group_slider .active .clothes', '#style_group_slider .active .digits_pager');
		}
	});

	//покажем лого если опустились достаточно низко
	$window.on('scroll', function() {
		teaser = $('#teaser');
		if(teaser.length > 0){
			$logo_small = $('#nav .logo');
				teaserOffset = teaser.offset().top + teaser.outerHeight();
			if (_windowST >= teaserOffset) {
				$('#nav').addClass('fixed');
				$logo_small.fadeIn(500);
			} else {
				$('#nav').removeClass('fixed');
				$logo_small.fadeOut(500);
			}
		}
	});

	$('.item .plus').click(function(){
		i = $(this).closest('.item').index();
		$(this).closest('.item').toggleClass('disabled');
		d = $(this).closest('.calc').find('.disabled').length;
		if(d == 0)
			$(this).closest('.calc').addClass('fullstack');
		else
			$(this).closest('.calc').removeClass('fullstack');
	});

	$('.item .f_color input').change(function() {
		$label = $(this).closest('label');
		if($(this).is(":checked")){
			$(this).closest('.col-right').find('label').removeClass('checked');
			$label.addClass('checked');
		}
		else
			$label.removeClass('checked');
	});

	$('#order').click(function() {
		$('#form_box').show();
	})
	$('.modal .close').click(function() {
		$(this).closest('.modal').hide();
	})
	$('#menu span.link').click(function() {
		if(!$(this).hasClass('line')){
			$('#menu span.link').removeClass('line');
			$('#menu .text').slideUp();
			$(this).toggleClass('line');
			$(this).next('.text').slideDown();
		}else{
			$(this).removeClass('line');
			$(this).next('.text').slideUp();
		}
	})
	$('#menu_toggler').click(function(e) {
		$('#menu').show()
	});

	$('.head + .bx-wrapper').height($('.style_group_slide').height());
});